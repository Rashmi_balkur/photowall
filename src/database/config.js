import * as firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyAk-eFhegxcwLXcXB7Cme8RlPDvKZ1x7pE",
  authDomain: "photowall-ccbdf.firebaseapp.com",
  databaseURL: "https://photowall-ccbdf-default-rtdb.firebaseio.com",
  projectId: "photowall-ccbdf",
  storageBucket: "photowall-ccbdf.appspot.com",
  messagingSenderId: "337132617532",
  appId: "1:337132617532:web:b4cf8d0ed42fdec6c9e839",
  measurementId: "G-V9DVZ0HGQT"
};

firebase.initializeApp(firebaseConfig)

const database = firebase.database();

export {database}